package SAD_day_01;

//import java.awt.*;
//import java.awt.event.*;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.TextField;
import java.awt.Button;
import java.awt.BorderLayout;
import java.awt.Panel;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class MyCalculator extends Frame implements ActionListener {

    TextField tf;
    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, bpls, bmin, bdiv, bmul, beq, bsqrt, bDecimal, bOneDivX, bPercentage, bPlsMin, bClear, bDelete, bsuper, bZeroZero, bem2, bMpls, bMmin, bMS, bMR, bMC;
    double fno, sno, ans;
    String op1, op2, text;
    int turn1 = 0;

    MyCalculator() {
        super("Calculator");
        setSize(450, 520);
        setLocationRelativeTo(null);

        BorderLayout border = new BorderLayout();
        Panel p1 = new Panel();
        Panel p2 = new Panel();
        Panel p3 = new Panel();
        Panel p4 = new Panel();
        Panel p5 = new Panel();
        add(p1, border.NORTH);
        add(p2, border.CENTER);
        add(p3, border.WEST);
        add(p4, border.SOUTH);
        add(p5, border.EAST);
        Color c = new Color(102, 102, 102);
        p1.setBackground(c);
        p2.setBackground(c);
        p3.setBackground(c);
        p4.setBackground(c);
        p5.setBackground(c);

        Font ft = new Font("Tahoma", 1, 30);
        tf = new TextField("0", 23);
        tf.setFont(ft);
        tf.setEditable(false);
        p1.add(tf);

        GridLayout grid = new GridLayout(6, 5, 5, 10);
        p2.setLayout(grid);

        b1 = new Button("1");
        b2 = new Button("2");
        b3 = new Button("3");
        b4 = new Button("4");
        b5 = new Button("5");
        b6 = new Button("6");
        b7 = new Button("7");
        b8 = new Button("8");
        b9 = new Button("9");
        b0 = new Button("0");
        bpls = new Button("+");
        bmin = new Button("-");
        bmul = new Button("*");
        bdiv = new Button("/");
        bDecimal = new Button(".");
        beq = new Button("=");
        bOneDivX = new Button("1/x");
        bPercentage = new Button("%");
        bPlsMin = new Button("+/-");
        bClear = new Button("C");
        bDelete = new Button("DEL");
        bsuper = new Button("x^2");
        //  bsuper.setLabel("<html><b>x<sup>2</sup></b></html>");
        bZeroZero = new Button("00");
        bem2 = new Button("x!");
        bMpls = new Button("M+");
        bMmin = new Button("M-");
        bMS = new Button("MS");
        bMR = new Button("MR");
        bMC = new Button("MC");
        bsqrt = new Button("\u221A");

        p2.add(bMC);
        p2.add(bMR);
        p2.add(bMS);
        p2.add(bMpls);
        p2.add(bMmin);
        p2.add(bDelete);
        p2.add(bClear);
        p2.add(bem2);
        p2.add(bPlsMin);
        p2.add(bsqrt);
        p2.add(b7);
        p2.add(b8);
        p2.add(b9);
        p2.add(bdiv);
        p2.add(bPercentage);
        p2.add(b4);
        p2.add(b5);
        p2.add(b6);
        p2.add(bmul);
        p2.add(bOneDivX);
        p2.add(b1);
        p2.add(b2);
        p2.add(b3);
        p2.add(bmin);
        p2.add(bsuper);
        p2.add(b0);
        p2.add(bZeroZero);
        p2.add(bDecimal);
        p2.add(bpls);
        p2.add(beq);

        Font f = new Font("Tahoma", 1, 18);
        Font f2 = new Font("Tahoma", 1, 30);
        b1.setFont(f);
        b2.setFont(f);
        b3.setFont(f);
        b4.setFont(f);
        b5.setFont(f);
        b6.setFont(f);
        b7.setFont(f);
        b8.setFont(f);
        b9.setFont(f);
        b0.setFont(f);
        bpls.setFont(f);
        bmin.setFont(f2);
        bmul.setFont(f2);
        bdiv.setFont(f2);
        bDecimal.setFont(f2);
        beq.setFont(f);
        bOneDivX.setFont(f);
        bPercentage.setFont(f);
        bPlsMin.setFont(f);
        bClear.setFont(f);
        bDelete.setFont(f);
        bsuper.setFont(f);
        bZeroZero.setFont(f);
        bem2.setFont(f);
        bMpls.setFont(f);
        bMmin.setFont(f);
        bMS.setFont(f);
        bMR.setFont(f);
        bMC.setFont(f);
        bsqrt.setFont(f);

        Color c1 = new Color(0, 0, 0);
        b1.setBackground(c1);
        b2.setBackground(c1);
        b3.setBackground(c1);
        b4.setBackground(c1);
        b5.setBackground(c1);
        b6.setBackground(c1);
        b7.setBackground(c1);
        b8.setBackground(c1);
        b9.setBackground(c1);
        b0.setBackground(c1);
        bpls.setBackground(c1);
        bmin.setBackground(c1);
        bmul.setBackground(c1);
        bdiv.setBackground(c1);
        bDecimal.setBackground(c1);
        beq.setBackground(c1);
        bOneDivX.setBackground(c1);
        bPercentage.setBackground(c1);
        bPlsMin.setBackground(c1);
        bClear.setBackground(c1);
        bDelete.setBackground(c1);
        bsuper.setBackground(c1);
        bZeroZero.setBackground(c1);
        bem2.setBackground(c1);
        bMpls.setBackground(c1);
        bMmin.setBackground(c1);
        bMS.setBackground(c1);
        bMR.setBackground(c1);
        bMC.setBackground(c1);
        bsqrt.setBackground(c1);

        Color c2 = new Color(255, 255, 255);
        b1.setForeground(c2);
        b2.setForeground(c2);
        b3.setForeground(c2);
        b4.setForeground(c2);
        b5.setForeground(c2);
        b6.setForeground(c2);
        b7.setForeground(c2);
        b8.setForeground(c2);
        b9.setForeground(c2);
        b0.setForeground(c2);
        bpls.setForeground(c2);
        bmin.setForeground(c2);
        bmul.setForeground(c2);
        bdiv.setForeground(c2);
        bDecimal.setForeground(c2);
        beq.setForeground(c2);
        bOneDivX.setForeground(c2);
        bPercentage.setForeground(c2);
        bPlsMin.setForeground(c2);
        bClear.setForeground(c2);
        bDelete.setForeground(c2);
        bsuper.setForeground(c2);
        bZeroZero.setForeground(c2);
        bem2.setForeground(c2);
        bMpls.setForeground(c2);
        bMmin.setForeground(c2);
        bMS.setForeground(c2);
        bMR.setForeground(c2);
        bMC.setForeground(c2);
        bsqrt.setForeground(c2);

        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        b4.addActionListener(this);
        b5.addActionListener(this);
        b6.addActionListener(this);
        b7.addActionListener(this);
        b8.addActionListener(this);
        b9.addActionListener(this);
        b0.addActionListener(this);
        bpls.addActionListener(this);
        bmin.addActionListener(this);
        bdiv.addActionListener(this);
        bmul.addActionListener(this);
        beq.addActionListener(this);
        bDecimal.addActionListener(this);
        bOneDivX.addActionListener(this);
        bPercentage.addActionListener(this);
        bPlsMin.addActionListener(this);
        bClear.addActionListener(this);
        bDelete.addActionListener(this);
        bsuper.addActionListener(this);
        bZeroZero.addActionListener(this);
        bem2.addActionListener(this);
        bMpls.addActionListener(this);
        bMmin.addActionListener(this);
        bMS.addActionListener(this);
        bMR.addActionListener(this);
        bMC.addActionListener(this);
        bsqrt.addActionListener(this);

        MenuBar mb = new MenuBar();
        Menu menu1 = new Menu("File");
        MenuItem mi1 = new MenuItem("Open");
        menu1.add(mi1);
        MenuItem mi2 = new MenuItem("Close");
        menu1.add(mi2);
        Menu menu2 = new Menu("Edit");
        MenuItem mi1Menu = new MenuItem("Undo");
        menu2.add(mi1Menu);
        MenuItem mi2Menu = new MenuItem("Redo");
        menu2.add(mi2Menu);
        MenuItem mi3Menu = new MenuItem("Refresh");
        menu2.add(mi3Menu);
        Menu menu3 = new Menu("Help");
        MenuItem mi3 = new MenuItem("VIew Help F1");
        menu3.add(mi3);
        MenuItem mi4 = new MenuItem("About Calculator");
        menu3.add(mi4);
        mb.add(menu1);
        mb.add(menu2);
        mb.add(menu3);

        setMenuBar(mb);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        //bsqrt.addActionListener(e -> getSqrt());
        bsqrt.addActionListener(e -> {
            fno = Double.parseDouble(tf.getText().trim());
            ans = Math.sqrt(fno);
            tf.setText("" + ans);
            fno = 0;
            ans = 0;
        });
        bsuper.addActionListener(e -> {
            fno = Double.parseDouble(tf.getText().trim());
            ans = fno * fno;
            tf.setText("" + ans);
            fno = 0;
            ans = 0;
        });
    }

    public void actionPerformed(ActionEvent e) {

        Object btn = e.getSource();
        text = e.getActionCommand();

        if (fno == 0.0) {
            if (btn.equals(b1) || btn.equals(b2) || btn.equals(b3) || btn.equals(b4) || btn.equals(b5) || btn.equals(b6) || btn.equals(b7) || btn.equals(b8) || btn.equals(b9) || btn.equals(b0)
                    || btn.equals(bZeroZero)) {
                startValues();
            } else if (btn.equals(bpls) || btn.equals(bmin) || btn.equals(bdiv) || btn.equals(bmul)) {
                turn1 = 0;
                fno = Double.parseDouble(tf.getText().trim());
                op1 = text;
                tf.setText("0");
            }
        } else {
            if (btn.equals(b1) || btn.equals(b2) || btn.equals(b3) || btn.equals(b4) || btn.equals(b5) || btn.equals(b6) || btn.equals(b7) || btn.equals(b8) || btn.equals(b9) || btn.equals(b0)
                    || btn.equals(bZeroZero)) {
                startValues();
            } else if (btn.equals(bpls) || btn.equals(bmin) || btn.equals(bdiv) || btn.equals(bmul) || btn.equals(beq)) {
                turn1 = 0;
                sno = Double.parseDouble(tf.getText().trim());
                if (btn.equals(beq)) {
                    calculation();
                    tf.setText("" + ans);
                } else {
                    calculation();
                    tf.setText("0");
                }
            }
        }

        if (btn.equals(bDelete)) {
            tf.setText("0");
            deleteProccess();
        }

        if (btn.equals(bClear)) {

            String value = tf.getText().trim();
            if (!value.equals("0") && !value.equals("")) {
                tf.setText(value.substring(0, tf.getText().length() - 1));
                if ((tf.getText().length() - 1) == -1) {
                    tf.setText("0");
                    deleteProccess();
                }
            } else {
                tf.setText("0");
            }
        }
        if (btn.equals(bDecimal)) {

            if (turn1 == 0) {
                double val = 0;
                if (!tf.getText().trim().equals("")) {
                    val = Double.parseDouble(tf.getText().trim());
                }
                if (val != 0.0) {
                    tf.setText(tf.getText().trim() + text);
                } else {
                    tf.setText(tf.getText().trim() + text);
                }
                turn1 = 1;
            }
        }

        if (btn.equals(bPlsMin)) {

            if (turn1 == 0) {
                text = "-";
                double val = 0;
                if (!tf.getText().trim().equals("")) {
                    val = Double.parseDouble(tf.getText().trim());
                }
                if (val != 0.0) {
                    if (!text.equals("-")) {
                        tf.setText(tf.getText().trim() + text);
                    } else {
                        tf.setText(text + tf.getText().trim());
                    }
                } else {
                    tf.setText(text);
                }
                turn1 = 1;
            }
            // startValues();
        }

        if (btn.equals(bPercentage)) {

            if (op1 != null) {
                turn1 = 0;
                sno = Double.parseDouble(tf.getText().trim());
                calculation();
                double finalAns = ans * 100;
                tf.setText(finalAns + "");
            }
            if (!tf.getText().trim().equals("")) {
                if (tf.getText().trim().equals("0")) {
                    tf.setText(text);
                } else if (tf.getText().trim().equals("%")) {
                } else {
                    op1 = text;
                    if (op1.equals(text)) {
                        fno = Double.parseDouble(tf.getText().trim());
                        ans = fno / 100;
                        tf.setText(ans + text);
                        // tf.setText(tf.getText().trim() + text);
                        op1 = null;
                    }
                }
            }
        }

        if (btn.equals(bOneDivX)) {
            fno = Double.parseDouble(tf.getText().trim());
            ans = 1 / fno;
            tf.setText("" + ans);
        }

        if (btn.equals(bem2)) {
            if (!tf.getText().trim().equals("0")) {
                ans = 1;
                fno = Double.parseDouble(tf.getText().trim());
                for (int x = 1; fno >= x; x++) {
                    ans = ans * x;
                }
                tf.setText("" + ans);
            }
            fno = 0.0;
        }

    }

    public void startValues() {
        double val = 0;
        if (!tf.getText().trim().equals("")) {
            if (!tf.getText().trim().equals("-")) {
                val = Double.parseDouble(tf.getText().trim());
            } else {
                val = -1;
            }
        }
        if (val != 0 || tf.getText().equals("0.")) {
            tf.setText(tf.getText().trim() + text);
        } else {
            tf.setText(text);
            if (text.equals("00")) {
                tf.setText("0");
            }
        }
    }

    public void deleteProccess() {
        fno = 0.0;
        sno = 0.0;
        ans = 0.0;
        op1 = null;
        op2 = null;
        turn1 = 0;
    }

    public void calculation() {
        switch (op1) {
            case "+":
                ans = fno + sno;
                break;
            case "-":
                ans = fno - sno;
                break;
            case "/":
                ans = fno / sno;
                break;
            case "*":
                ans = fno * sno;
                break;
        }
        fno = ans;
        op2 = text;
        op1 = op2;
    }

//    public void bClearActionPerformed(ActionEvent e) {
//        String ar = tf.getText();
//    }
}

class RunMyCal {

    public static void main(String args[]) {

        MyCalculator mycal = new MyCalculator();
        mycal.setVisible(true);

    }
}
